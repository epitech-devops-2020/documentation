# Raspberry cluster

Personnal cluster composed by 4 raspberry pi.

For external access you must use VPN.
Cluster is exposed behind `mancellin.fr` domain.

## Hardware

Harware configuration.

### Raspberry

The cluster isn't composed by only one type of raspberry.
Check table for hardware informations.

| Hostname | Raspberry model | CPU | CPU Core | RAM (MB)|
| --- | --- | --- | --- | --- |
| `192.168.0.58` | Pi 3 Model B Plus Rev 1.3 | ARMv7|  4 | 926 |
| `192.168.0.59` | Pi 3 Model B Plus Rev 1.3 | ARMv7 | 4 | 926 |
| `192.168.0.60` | Pi 3 Model B Rev 1.2 | ARMv7 | 4 | 926 |
| `192.168.0.61` | Pi Model B Plus Rev 1.2 | ARMv6 | 1 | 432 |

/!\ `192.168.0.61` is unused in cluster. Hardward of this board it's too low.

### Other

For turn-on cluster we need to use additional components.

| Brand | Model | Utility |
| --- | --- | --- |
| TP-link |  | Ethernet switch|
| Anker |  | Power |

## Architecture

One raspberry it's dedicated for routing.

Services on network raspberry:
- Openvpn
- Reverse proxy (not set)

The others are a kubernetes cluster, is composed by 3 raspberry.

- `192.168.0.58`
- `192.168.0.59`
- `192.168.0.60`

## Services

### OpenVPN

We use openvpn in docker container.
You can find how to use on [Github](https://github.com/giggio/docker-openvpn-arm)

#### Quick start

Create new VPN user
```
docker exec -it openvpn easyrsa build-client-full CLIENTNAME nopass
```

Get user VPN configuration
```
docker exec -it openvpn ovpn_getclient CLIENTNAME > CLIENTNAME.ovpn
```

Show users list
```
docker exec -it openvpn ovpn_listclients
```

Revoke a user
```
docker exec -it openvpn easyrsa revoke client1
docker exec -it openvpn easyrsa gen-crl
```
